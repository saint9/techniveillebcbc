class Interaction {

    constructor(options) {

        this.initObjects();
        this.initFunction();
        this.initEvents()
        this.init()

    }



    initObjects(){

        this.intro = document.getElementsByClassName("intro");
        this.introContent = document.getElementsByClassName("main-content");
        this.introDescription = document.getElementsByClassName("intro-description");
        this.toIndex = document.getElementsByClassName("to-index");
        this.index = document.getElementsByClassName("index");
        this.chips = document.getElementsByClassName("chip");
        this.notes = {
            all : document.getElementsByClassName("categoriesNames")[0],
            arduino : {
              location : document.getElementsByClassName("categoriesNames")[0].getElementsByClassName('arduino'),
              sound: document.getElementById('sound1')
            },
            printing : {
              location : document.getElementsByClassName("categoriesNames")[0].getElementsByClassName('printing'),
              sound: document.getElementById('sound2')
            },
            bending : {
              location : document.getElementsByClassName("categoriesNames")[0].getElementsByClassName('bending'),
              sound: document.getElementById('sound3')
            },
            others : {
              location : document.getElementsByClassName("categoriesNames")[0].getElementsByClassName('others'),
              sound: document.getElementById('sound4')
            },
        }

        this.current = {
            chip : '',
            article : '',
            presentationCategory: ''
        }
        this.inChange = false;
        this.firstIntroClicked = false;
        this.inIndex = false;


    }

    initEvents(){

        this.introContent[0].addEventListener('click', (e) =>{

            if(this.firstIntroClicked == false){

                    this.firstIntroClicked = true;
                    var tween = TweenMax.to(this.introContent[0], 1, {opacity: 0, display: 'none', onComplete: () => {
                        this.introContent[0].classList.remove('full-screen');

                        var tween = TweenMax.to(this.introContent[0], 1, {opacity: 1, display: 'table'});
                        var tween = TweenMax.to(this.introDescription[0], 1, {opacity: 1, display: 'block'});
                        var tween = TweenMax.to(this.toIndex[0], 1, {opacity: 1, display: 'block'});

                    }});
            }


        });

        this.toIndex[0].addEventListener('click', (e) =>{

            this.index[0].style.display = 'block';
            var tween = TweenMax.to(this.intro[0], 1, {y : '-100%', height : 0, onComplete: () =>{

                this.inIndex = true;

            }});


        });

//        for (var i = 0; i < this.chips.length; i++) {
//
//            var chip = this.chips[i];
//
//            chip.addEventListener('click', (e) => {
//
//                var position = e.pageY - 130;
//
//                window.scrollTo(0, position);
//
//            });
//
//        };

        window.addEventListener('scroll', (e) =>{

        var timelines = document.getElementsByClassName('timelines-categories')[0];

        if(timelines.getBoundingClientRect().top < 85){

            this.notes.all.classList.add('scrolledStatut');

        }else{

            this.notes.all.classList.remove('scrolledStatut');

        }

        for (var i = 0; i < this.chips.length; i++) {

            var chip = this.chips[i];


            if(chip.getBoundingClientRect().top < 120){

                if(!chip.classList.contains('hidden')){

                    var type = chip.getAttribute('data-type');

                    var all = this.notes.all.getElementsByClassName('active');
                    for(var i = 0; i < all.length; i++){

                        all[i].classList.remove('active');

                    }
                    this.notes[type].location[0].classList.add('active');



                    this.notes[type].sound.currentTime = 0;
                    this.notes[type].sound.play();


                    chip.classList.add('hidden');

                    if(this.inChange == false){
                        this.inChange = true;

                        if(this.current.chip != ''){
                            this.current.chip.classList.remove('active');
                        }
                        this.current.chip =  chip;
                        this.current.chip.classList.add('active');


                        var index = chip.getAttribute("data-index");
                        var type = chip.getAttribute("data-type");

                        var target = document.getElementsByClassName(type)[index -1];
                        var parentTargetCategory = document.getElementsByClassName('presentation-contents');
                        var targetCategory = parentTargetCategory[0].getElementsByClassName(type);


                        if(this.current.article != ''){

                            var t = TweenMax.fromTo(this.current.presentationCategory, 0.3, {top: 180, opacity: 1}, {top: 250, opacity: 0, display: 'none', onComplete: () => {

                                var tween = TweenMax.fromTo(targetCategory, 0.3, {top: 250, opacity: 0, display: 'block'}, {top: 180, opacity: 1});
                                this.current.presentationCategory = targetCategory;

                            }});

                            var tween = TweenMax.fromTo(this.current.article, 0.3, {top: 180, opacity: 1}, {top: 250, opacity: 0, display: 'none', onComplete: () => {

                                var tween = TweenMax.fromTo(target, 0.3, {top: 250, opacity: 0, display: 'block'}, {top: 180, opacity: 1});
                                this.current.article = target;
                                this.inChange = false;

                            }});
                        }else{

                            var tween = TweenMax.fromTo(target, 0.3, {top: 250, opacity: 0, display: 'block'}, {top: 180, opacity: 1, onComplete: () => {

                                this.current.article = target;
                                this.inChange = false;

                            }});

                            if( !this.current.presentationCategory == targetCategory){

                                var tween = TweenMax.fromTo(targetCategory, 0.3, {top: 250, opacity: 0, display: 'block'}, {top: 180, opacity: 1, onComplete: () => {

                                    this.current.presentationCategory = targetCategory;
                                    this.inChange = false;

                                }});


                            }

                        }




                    }





                }

            }else{

                if(chip.classList.contains('hidden')){



                    chip.classList.remove('hidden');

                }

            }

        }

        });




    }

    initFunction(){


    }

    init() {




    }

    update(){

    }
}


export default Interaction
