class Smooth_scroll {

    constructor(options) {

            this.options                = options                           || {}
            this.damping                = 1 / this.options.damping          || .1
            this.speed                  = this.options.speed                || 50
            this.scroll_target          = 0
            this.SCROLL                 = 0
            this.last_pass              = Date.now()

            this.init()
            this.get_bounding()
    }

    init() {

            var that = this

            window.addEventListener('mousewheel', function(evt){
                that.on_scroll(evt)
                evt.preventDefault()
                return false
            }, false)

            window.addEventListener('DOMMouseScroll', function(evt){
                that.on_scroll(evt)
                evt.preventDefault()
                return false
            }, false)

            window.addEventListener('resize', function(){
                that.get_bounding()
            }, false)

            this.update()

    }

    get_bounding(){

            var B = document.body,
                H = document.documentElement

            if (typeof document.height !== 'undefined') {
                this.HEIGHT = document.height // For webkit browsers
            } else {
                this.HEIGHT = Math.max( B.scrollHeight, B.offsetHeight,H.clientHeight, H.scrollHeight, H.offsetHeight )
            }

            this.HEIGHT += 500;

    }

    on_scroll(evt){

            var e = window.event || evt
            var delta = Math.max(-1, Math.min(1, -e.wheelDelta || e.detail));

            var t = Date.now()
            if (t - this.last_pass > 15) {
                this.scroll_target += delta * this.speed
                this.scroll_target = Math.min(Math.max(this.scroll_target, 0), this.HEIGHT)
                this.last_pass = t
            }

    }

    update(){
        var that = this
        requestAnimationFrame(function() {
            that.update()
        })

        var diff_scroll = this.scroll_target - this.SCROLL
        diff_scroll *= this.damping
        this.SCROLL += diff_scroll
        window.scroll(0, this.SCROLL)

    }
}

export default Smooth_scroll
