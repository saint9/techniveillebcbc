import * as TweenMax from 'gsap';
import Interaction from './components/interaction.class.js'
import Smooth_scroll from './components/scroll_handler.class.js'

var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,                    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    isFirefox = typeof InstallTrigger !== 'undefined',                                        // Firefox 1.0+
    isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0, // At least Safari 3+: "[object HTMLElementConstructor]"
    isChrome = !!window.chrome && !isOpera,                                                   // Chrome 1+
    isIE = /*@cc_on!@*/false || !!document.documentMode,                                    // At least IE6
    isEdge = navigator.userAgent.indexOf(' Edge/') >= 0;


var interaction = new Interaction();
var smoothScroll = new Smooth_scroll({

    damping: 5,
    speed : 5

});

animate();

function animate() {
    requestAnimationFrame(animate);

//    background.update()


}