<?php global $nameProject; ?>
<?php get_header(); ?>

<?php


$categories = get_terms('category');

$posts = array();

foreach ($categories as $category){

	$args = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'meta_key'			=> 'date',
		'orderby'			=> 'meta_value',
		'order'				=> 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => $category->slug,
			)
		)
	);

	$categoryPost = get_posts($args);

	${$category->slug.'Posts'} = $categoryPost;


	foreach (${$category->slug.'Posts'} as $post){

		$date = DateTime::createFromFormat('d/m/Y', get_field('date', $post->ID));

		$object = array(
			'title' => get_field('title', $post->ID),
			'date' => $date,
			'description' => get_field('description', $post->ID),
			'image' => get_field('image', $post->ID),
			'link' => get_field('link', $post->ID),
			'category' => $category->slug
		);

		array_push($posts, $object);

	}


}

function cmp($a, $b)
{
	if ($a['date'] == $b['date']) {
		return 0;
	}

	return ($a['date'] < $b['date']) ? -1 : 1;

}



	usort($posts, "cmp");



$firstDate =  DateTime::createFromFormat('d/m/Y', '01/09/2016');
$lastDate =  DateTime::createFromFormat('d/m/Y', '31/03/2017');


?>

<div class="intro">


	<div class="main-content full-screen">


		<h1>
			DIY
		</h1>
		<h2>
			music
		</h2>


	</div>

		<p class="intro-description">

			Fabrication of musical instrument usually require a high level of skills and expertise. Traditionally, instruments are made of wood, copper, with parts that vibrates, and other that resonates, until the 50’s where were invented the electronic music. The sounds of electronic music was created only with computers. It became a lot easier to create sounds by modifying parameters on the computers. And soon, almost everyone became able to be a musician and create their own sounds and instruments. But it require time, money and knowledge.
			Today, the microchip became a lot smaller, uniformed, and all the equipment needed got really cheap. So everyone can have access to the basic elements to create his own music. And thanks to the internet, all the knowledge you need is within reach.
		</p>

	<div class="to-index">


	</div>





</div>

<div class="index">



	<section class="timeline">

		<div class="flux">

			<div class="article-contents">


				<?php foreach ($posts as $post): ?>


					<article class="content <?php echo $post['category']; ?>">



						<div class="illustration" style="background-image: url('<?php echo $post['image']['url']; ?>') "></div>

						<h2 class="title">

							<?php echo $post['title']; ?>

						</h2>

						<p class="date">

							<?php

							$stringDate =  date_i18n('d.m.y', strtotime($post['date']->format('m/d/Y'))); ?>
							<?php echo $stringDate; ?>

						</p>

						<a href="<?php echo $post['link']; ?>"><?php echo $post['link']; ?></a>

						<div class="separator">

						</div>

						<div class="description">
							<?php echo $post['description']; ?>
						</div>

					</article>

				<?php endforeach; ?>


			</div>



			<div class="timelines-categories">


				<div class="categoriesNames">


					<?php foreach ($categories as $category): ?>

						<div class="catName <?php echo $category->slug; ?>">

							<span>
								<?php echo $category->slug; ?>
							</span>

							<div class="after">

							</div>
						</div>



					<?php endforeach; ?>

				</div>


				<div class="months">

					<span class="month">Sept</span>
					<span class="month">Oct</span>
					<span class="month">Nov</span>
					<span class="month">Dec</span>
					<span class="month">Jan</span>
					<span class="month">Fev</span>
					<span class="month">Mar</span>
					<span class="month">Avr</span>


				</div>


				<?php foreach ($categories as $category): ?>

					<div class="timeline-single-group <?php echo $category->slug; ?>">


						<div class="timeline-single">

							<?php $i = 1;?>
							<?php foreach (${$category->slug.'Posts'} as $post): ?>
								<!---->
								<?php

								$date = get_field('date', $post->ID);
								$formatDate = DateTime::createFromFormat('d/m/Y', $date);

								$totalDays = $firstDate->diff($lastDate);
								$days = $firstDate->diff($formatDate);
								$porcent = ($days->days * 100) / $totalDays->days;


								?>
								<!---->
								<!---->
								<div class="chip" style="top: <?php echo $porcent.'%'; ?> " data-type="<?php echo $category->slug; ?>" data-index="<?php echo $i; ?>"></div>

								<!---->
								<?php $i++; ?>
							<?php endforeach; ?>

						</div>


					</div>


				<?php endforeach; ?>


			</div>

			<div class="presentation-contents">


				<?php foreach ($categories as $category): ?>

					<div class="presentation <?php echo $category->slug; ?>">

						<h2 class="title">

							<?php echo $category->name; ?>


						</h2>

						<div class="separator">

						</div>

						<div class="text">

							<?php echo $category->description; ?>

						</div>


					</div>

				<?php endforeach; ?>

			</div>


			<div class="pdf">
				<a href="<?php echo get_site_url(); ?>/wp-content/themes/default/sounds/sound1.mp3" target="_blank" >Download the PDF</a>

			</div>

		</div>

	</section>


</div>

<audio id="sound1" controls>
	<source src="<?php echo get_site_url(); ?>/wp-content/themes/default/sounds/sound1.mp3" type="audio/mpeg">
</audio>

<audio id="sound2" controls>
	<source src="<?php echo get_site_url(); ?>/wp-content/themes/default/sounds/sound2.mp3" type="audio/mpeg">
</audio>

<audio id="sound3" controls>
	<source src="<?php echo get_site_url(); ?>/wp-content/themes/default/sounds/sound3.mp3" type="audio/mpeg">
</audio>

<audio id="sound4" controls>
	<source src="<?php echo get_site_url(); ?>/wp-content/themes/default/sounds/sound4.mp3" type="audio/mpeg">
</audio>

<?php get_footer(); ?>
