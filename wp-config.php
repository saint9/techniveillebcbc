<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'techniveilleBCBC');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';%4 8|nG@2 +KBBrV&12P$Yf<UNar!jvdC+Vn8ihP fwpuQ<F_%jkLk(G[}P2peO');
define('SECURE_AUTH_KEY',  '#,udMmW2X3]L-SH;~c2t/$|EDlGew_U*jy!FKjS1}oT9X13K-F>P7Y#:roCKetBI');
define('LOGGED_IN_KEY',    'FAdQ1v6RNTkl)Z3F:/tl^y/QF%A]DKDgcm~dG^RQomBQ z1P69[{U^-ZiUE&uARi');
define('NONCE_KEY',        '^bXeKwSw#QbIE^)km9]ECNEdba-l07uv {sBxs|U0%[uCP3h[ljKxpDR.Z*qn@Gw');
define('AUTH_SALT',        ':B/H}F]*13|+X[nhBrV-gZfm#O1UM7Npo/ag,ckmp9>&(u6:s_5$oU0/q3T= .x ');
define('SECURE_AUTH_SALT', ')0djCWz*`ew!G+|yndn|Y`;{Y`b#6la@Y$95i|CM(0#^6fJc#|VC.aXPr:0#K87m');
define('LOGGED_IN_SALT',   '~.-AyXmIBWi/D]lF-LPdntbW3|HW:L1HXXtGfN e]h0dYkaXR*:3cxc=lYubP+!R');
define('NONCE_SALT',       '..Bs*if]&A:0=3av:[(fxvF(!iE2o P8h#Pc/O/t_67|G}-{,p`&K!%@}Zqy{?au');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_techniveilleBCBC';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');